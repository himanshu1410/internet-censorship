from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    #website = models.URLField(blank=True)#TODO
    #picture = models.ImageField(upload_to='profile_images', blank=True)#TODO

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username


class UrlData(models.Model):
    url = models.URLField(blank=True)
    ip = models.IPAddressField(blank=True)
    comments = models.CharField(max_length=500, blank= True)
    status = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s %s %s' % (self.url, self.ip, self.comments)