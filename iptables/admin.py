from django.contrib import admin
from iptables.models import UserProfile, UrlData

admin.site.register(UserProfile)
admin.site.register(UrlData)