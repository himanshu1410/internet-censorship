__author__ = 'dexter'
from django.conf.urls import patterns,url
from iptables import views

urlpatterns = patterns('',
                       #url(r'^$',views.index, name='index'),
                      # url(r'^login/$', views.user_login, name='login'),
                      url(r'^$', views.user_login, name='login'),
                      url(r'^login/$', views.user_login, name='login'),
                      url(r'^logout/$', views.user_logout, name='logout'),
                      url(r'^manage/$', views.manage, name='manage'),
                       )