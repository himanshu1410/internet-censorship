# Create your views here.
from django.template import RequestContext
from django.shortcuts import render_to_response

import iptc

from django.http import HttpResponse
# Import Data models
#from iptables.models import Category, Page
# Import Forms
#from iptables.forms import  CategoryForm, PageForm, UserForm, UserProfileForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse
import urllib
from django.contrib.auth import logout
#def index(request):
#  context = RequestContext(request)
#  category_list = Category.objects.order_by('-page')
#  context_dict = {'categories': category_list, 'boldmessage': "I am bold font from the context"}
#  return render_to_response('iptables/index.html',context_dict, context)
from iptables.models import UrlData
from iptables.forms import UrlDataForm
from django.contrib.auth.decorators import login_required

def index(request):
    # Obtain the context from the HTTP request.
    context = RequestContext(request)

    # Query for categories - add the list to our context dictionary.
    context_dict = 'No Data'

    # Render the response and return to the client.
    return render_to_response('iptables/index.html', context_dict, context)



def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    context_dict={}

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/iptables/manage')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your iptables account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            context_dict['error']={'Invalid login details. Please check your username and password.'}
            return render_to_response('iptables/login.html', {}, context)


    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('iptables/login.html', {}, context)


@login_required
def restricted(request):
    return HttpResponse("Since you're logged in, you can see this text!")


# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/iptables/')


 #Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def manage(request):
    # Since we know the user is logged in, we can now just log them out.
        # Like before, obtain the context for the user's request.
    context = RequestContext(request)
    url_list = UrlData.objects.all()

    context_dict={'url_data': url_list}
    print "-"*30
    print context_dict['url_data']
    form = UrlDataForm()
    context_dict['form'] = form
    print "-"*30

    if request.method == 'POST':

        form_data = UrlDataForm(request.POST)
        print "#"*30
        if form_data.is_valid():
            url_data_entry = form_data.save(commit=False)
            if request.POST['url']:
                input_url = request.POST['url']
                print "URL Entered:", input_url
                # Perform a check for valid IP address entered

            if request.POST['ip']:
                ip_address = request.POST['ip']
                print "IP address: ", ip_address

            if request.POST['comments']:
                comments = request.POST['comments']
                print "Comments", comments

            url_data_entry.status = True
            url_data_entry.save()

            rule = iptc.Rule()
            rule.protocol = "tcp"

            print "#"*30
        else:
            print "+"*30
            print form_data.errors
            return HttpResponse("Invalid login details supplied.")

    else:
        return render_to_response('iptables/manage.html', context_dict, context)
'''
#    print context_dict
#    context_dict['url_list'] = url_list
#    print "+"*30
#    print context_dict.__dict__


    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['URL']
        password = request.POST['IP']
        password = request.POST['comments']

        # Check well formed URL
        # Check well formed IP

        # Insert the entry in the database

        # Return the context with all the values

        # Display the value
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...


        # Get all the data from the database and pass it as dictionary object.
        return render_to_response('iptables/manage.html', context_dict, context)

    # Take the user back to the homepage.
    return HttpResponseRedirect('/iptables/')
    '''