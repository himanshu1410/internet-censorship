__author__ = 'dexter'
from django import forms
from iptables.models import UserProfile, UrlData
from django.contrib.auth.models import User

class UserForm(forms.ModelForm):
    username = forms.CharField(help_text="Please enter a username.")
    email = forms.CharField(help_text="Please enter your email.")
    password = forms.CharField(widget=forms.PasswordInput(), help_text="Please enter a password.")

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
#TODO
class UserProfileForm(forms.ModelForm):
    website = forms.URLField(help_text="Please enter your website.", required=False)
    picture = forms.ImageField(help_text="Select a profile image to upload.", required=False)

    class Meta:
        model = UserProfile
        fields = ['website', 'picture']


class UrlDataForm(forms.ModelForm):

    url = forms.URLField(required=False)
    ip = forms.IPAddressField(required=False)
    comments = forms.CharField(max_length=500, required=False)
    status = forms.CheckboxInput()

    class Meta:
        model = UrlData
        fields = ['url', 'ip', 'comments']